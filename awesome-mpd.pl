#! /usr/bin/env perl
use strict;
use warnings;
use IO::Socket;

my ($host, $port, $timeout) = ('localhost', '6600', '1');
my $DEBUG = 0;

sub sniffdattune {
	my $socket = IO::Socket::INET->new(Proto=>'TCP',PeerPort=>$port,PeerAddr=>$host,Timeout=>$timeout);
	return "MPD offline" if !$socket;
	my $state = "";
	print $socket "status\n";
	while(<$socket>) {
		last if /^(OK$|ACK)/;
		print $_ if $DEBUG;
		$state = "(PAUSED)" if /^state: (pause|stop)$/;
	}

	print $socket "currentsong\n";
	my ($artist, $album, $title, $year) = ("","","","");
	while (<$socket>) {
		last if /^OK$/;
		print "curr_resp: $_" if $DEBUG;
		if (/^Artist: (.+)$/) {
			$artist = $1;
		}
		if (/^Album: (.+)$/) {
			$album = $1;
		}
		if (/^Date: (.+)$/) {
			$year = $1;
		}
		if (/^Title: (.+)$/) {
			$title = $1;
		}	
	}

	return "$state $artist - $title"; # ($album ($year)]";
}

my $output = sniffdattune();
print $output;
