#! /usr/bin/env bash

WIDGET="nowplaying"
SCROLL=1
S_OFF=0
S_CNT=30

OUTPUT="$(perl awesome-mpd.pl) "
while :; do 
	CURR_OUTPUT="$(perl awesome-mpd.pl) "
	OUTPUT=$CURR_OUTPUT

	#if [[ "$OUTPUT" == "$CURR_OUPUT" ]]; then
	#else
	#fi

	L=${#OUTPUT}
	if [ $SCROLL ]; then 
		if [ $S_CNT -gt $L ]; then S_CNT=$L; fi
		#echo "L=$L OFS=$S_OFF CNT=$S_CNT" 
		if [ $S_CNT -lt $L ]; then
			# OVERFLOW?
			if [ $(($S_CNT+$S_OFF)) -gt $L ]; then
				PART_A=${OUTPUT:$S_OFF:$(($L-$S_OFF))}
				PART_B=${OUTPUT:0:$(($S_CNT-${#PART_A}))}
				RESULT="$PART_A$PART_B"
			else
				RESULT=${OUTPUT:$S_OFF:$S_CNT}
			fi
			if [ $S_OFF -eq $L ]; then
				S_OFF=0 
			else
				S_OFF=$(($S_OFF+1))
			fi
		else
			RESULT=$OUTPUT
		fi 
			
	else
		RESULT=$OUTPUT
	fi

	echo "$WIDGET:set_text(\"$RESULT\")" | awesome-client
	sleep 0.4
done
